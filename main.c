/*
Author: Simon Podlesny (xpodle01)

Description: Program will process text from standard input and will determine if input is number, prime number, date, palindrome or text.x
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <limits.h>
#include <errno.h>
#include <ctype.h>
#include <time.h>

const char description[] = "Usage: ./proj1 < input.txt \nDescription: Program process text from standard input and determine if input is number, prime number, date, palindrome or text.\nAuthor: Simon Podlesny (xpodle01)\n";


/*
Determine the string length. Same as strlen() but strlen() is in string.h library

Input: string
Output: int
*/
int get_len(char *word)
{
    int lenght = 0;
    while(word[lenght] != '\0')
    {
        lenght++;
    }
    return lenght;
}


/*
Determine if input is number or not

Input: string
Output: bool
*/
bool is_number(char *word)
{
    char *end = NULL;

    strtol(word, &end, 10);
    if (get_len(end) > 0)
    {
        return false;
    }
    else if(strtol(word, &end, 10) >= 0 && word[0] != '+'){
        return true;
    }
    else{
        return false;
    }
    
}


/*

Function will determine if number is in range (bigger then 0 and less then INT_MAX) so it can be handle by is_prime() funtion.
This funtion do not verify input. Use is_number() for control of input!

Input: string
Output: bool

*/
bool is_in_range(char *word)
{
    char *end = NULL;
    int number = strtol(word, &end, 10);

    //Check if number is out of range (int)
    if (errno == ERANGE)
    {
        errno = 0;
        return false;
    }
    else if(number <= INT_MAX && number > 1) 
    {
        return true;
    }
    else
    {
        return false;
    }
}


/*
Convert string value into integer.
Function do not verify input. Use is_number() to verify input!

Input: char
Output: int
*/
int to_integer(char *word)
{
    char *end = NULL;
    return strtol(word, &end, 10);
}


/*
Determine if input is prime number.

Input: string
Output: bool
*/
bool is_prime(char *word)
{
    int n = to_integer(word);
    for(int i=2; i<=n/2; ++i)
    {
        if(n%i==0)
        {
            return false;
        }
    }
    return true;
}


/*
Determine if input is palindrome or not.

Input: string
Output: bool
*/
bool is_palindrome(char *word)
{
    int begin, end, lenght = 0;

    while(word[lenght] != '\0')
    {
        lenght++;
    }

    end = lenght-1;

    for(begin = 0; begin < end; begin++)
    {
        if(word[begin] != word[end])
        {
            return false;
        }
        end--;

    }
    return true;

}


/*
Determine if format of string is the same as date format (YYYY-MM-DD)

Input: string
Output: bool
*/
bool is_date(char *word) 
{

    //Check for lenght of string
    if(get_len(word) != 10) return false;

    //Check if char on position 0 - 3 is number
    if(isdigit(word[0]) == 0) return false;
    if(isdigit(word[1]) == 0) return false;
    if(isdigit(word[2]) == 0) return false;
    if(isdigit(word[3]) == 0) return false;

    //Check if year is bigger then 1900 (limitation of struct tm) 
    char year [] = {word[0], word[1], word[2], word[3], '\0'};
    if (to_integer(year) < 1900) return false;

    //Check if char on position is -
    if(word[4] != '-') return false;

    //Check if char on position 5 - 6 is number
    if(isdigit(word[5]) == 0) return false;
    if(isdigit(word[6]) == 0) return false;

    //Check if month do not exceed number 12
    char month [] = {word[5], word[6], '\0'};
    if(to_integer(month) > 12) return false;

    //Check if char on position is -
    if(word[7] != '-') return false;

    //Check if char on position 8 - 9 is number
    if(isdigit(word[8]) == 0) return false;
    if(isdigit(word[9]) == 0) return false;

    //Check if day do not exceed 31
    char day [] =  {word[8], word[9], '\0'};
    if(to_integer(day) > 31) return false;

    return true;
}


/*
Print "Mon, Tue, Wed,..." from date format YYYY-MM-DD

Input: string
Output: void
*/
bool get_date(char *raw_date, char *day_by_name)
{
    char year [] = {raw_date[0], raw_date[1], raw_date[2], raw_date[3], '\0'};
    char month [] = {raw_date[5], raw_date[6], '\0'};
    char day [] =  {raw_date[8], raw_date[9], '\0'};

    struct tm date = {
        .tm_year = to_integer(year)-1900,
        .tm_mon = to_integer(month)-1,
        .tm_mday = to_integer(day)
    };
    mktime(&date);

    if(strftime(day_by_name, 4, "%a" , &date)) //%a for Mon, Tue, ...
    {
        return true;
    }
    else{
    	return false;
    }
}


int main(int argc, char *argv[])
{
	(void) argv; //to avoid compiller warning
    if (argc > 1)
    {
        printf("%s", description);
        return 0;
    }

    else
    {
        char vstup[101]; 
        char day_by_name [4];

        while(scanf("%100s", vstup) != EOF)
        {
            if (is_number(vstup))
            {
                if(is_in_range(vstup))
                {
                    if(is_prime(vstup))
                    {
                        printf("number: %d (prime)\n", to_integer(vstup));
                    }
                    else
                    {
                        printf("number: %d\n", to_integer(vstup));
                    }
                }
                else
                {
                    printf("number: %s\n", vstup);
                }
            }
            else
            {
                if(is_palindrome(vstup))
                {
                    printf("word: %s (palindrome)\n", vstup);
                }
                else if(is_date(vstup))
                {
                	if (get_date(vstup, &day_by_name[0])){
                		printf("date: %s %s\n",day_by_name, vstup );
                	}
                	else{
                		printf("word: %s\n", vstup);
                	}
                }
                else
                {
                    printf("word: %s\n", vstup);
                }
            }
        }
        return 0;
    }
}
